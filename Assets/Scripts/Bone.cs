﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bone : MonoBehaviour {
    [SerializeField]
    public Transform boneCylinderPrefab;

    public Knuckle knuckleA;
    public Knuckle knuckleB;

    private GameObject bone;

    public float sizeConstraint;

	// Use this for initialization
	void Awake () {
        Vector3 positionA = knuckleA.transform.position;
        Vector3 positionB = knuckleB.transform.position;

        InstantiateBoneCylinder();
        UpdateBonePosition(positionA, positionB);
        sizeConstraint = (positionB - positionA).magnitude;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 positionA = knuckleA.transform.position;
        Vector3 positionB = knuckleB.transform.position;

        UpdateBonePosition(positionA, positionB);
    }

    private void InstantiateBoneCylinder()
    {
        bone = Instantiate<GameObject>(boneCylinderPrefab.gameObject, Vector3.zero, Quaternion.identity);
        UpdateBonePosition(knuckleA.transform.position, knuckleB.transform.position);
    }

    private void UpdateBonePosition(Vector3 beginPoint, Vector3 endPoint)
    {
        Vector3 line = endPoint - beginPoint;
        Vector3 center = beginPoint + (line / 2.0f);

        bone.transform.position = center;
        bone.transform.localRotation = Quaternion.FromToRotation(Vector3.up, line);
        //bone.transform.LookAt(beginPoint);

        Vector3 localScale = bone.transform.localScale;
        localScale.y = line.magnitude/2;
        bone.transform.localScale = localScale;
    }
}
