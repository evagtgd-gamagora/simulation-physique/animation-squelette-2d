﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : MonoBehaviour {

    Dictionary<Knuckle, List<Bone>> skeleton = new Dictionary<Knuckle, List<Bone>>();    

    void Awake () {
        GameObject[] bones = GameObject.FindGameObjectsWithTag("bone");

        foreach(GameObject go in bones)
        {
            Bone bone = go.GetComponent<Bone>();
            if (bone != null)
            {
                AddBone(bone);
            }
        }
    }

    private void AddBone(Bone bone)
    {
        AddSkeletonEntry(bone, bone.knuckleA);
        AddSkeletonEntry(bone, bone.knuckleB);
    }

    private void AddSkeletonEntry(Bone bone, Knuckle knuckle)
    {
        if (!skeleton.ContainsKey(knuckle))
        {
            skeleton.Add(knuckle, new List<Bone>());
        }
        
        if(!skeleton[knuckle].Contains(bone))
        {
            skeleton[knuckle].Add(bone);
        }
    }

    private Knuckle GetOtherKnuckle(Bone bone, Knuckle knuckle)
    {
        if (bone == null)
            return null;

        Knuckle otherKnuckle = bone.knuckleA;
        if (otherKnuckle == knuckle)
            otherKnuckle = bone.knuckleB;

        return otherKnuckle;
    }

    private Bone GetOtherBone(Knuckle knuckle, Bone bone)
    {
        if (knuckle == null)
            return null;

        foreach(Bone b in skeleton[knuckle])
        {
            if (b != bone)
                return b;
        }

        return null;
    }

    private List<Bone> RecursivePath(Knuckle startKnuckle, Knuckle endKnuckle, List<Bone> path)
    {
        if(path == null)
            path = new List<Bone>();

        if (startKnuckle == endKnuckle)
            return path;

        foreach (Bone b in skeleton[startKnuckle])
        {
            if(!path.Contains(b))
            {
                List<Bone> tempPath = new List<Bone>();
                tempPath.AddRange(path);
                tempPath.Add(b);
                Knuckle tempKnuckle = GetOtherKnuckle(b, startKnuckle);
                List<Bone> subPath = RecursivePath(tempKnuckle, endKnuckle, tempPath);

                if ((subPath[subPath.Count - 1].knuckleA == endKnuckle) || (subPath[subPath.Count - 1].knuckleB == endKnuckle))
                {
                    return subPath;
                }
            }
        }

        return path;
    }


    private float? GetDistanceToStaticKnuckle(Knuckle knuckle, Bone bone)
    {
        if (knuckle == null || bone == null)
            return null;

        float dist = 0;

        while(!knuckle.statik)
        {
            dist += bone.sizeConstraint;
            bone = GetOtherBone(knuckle, bone);
            if (bone == null)
                return null;
            knuckle = GetOtherKnuckle(bone, knuckle);
        }

        return dist;
    }

    private Knuckle GetStaticKnuckle(Knuckle knuckle, Bone bone)
    {
        if (knuckle == null)
            return null;

        while (!knuckle.statik)
        {
            bone = GetOtherBone(knuckle, bone);
            if (bone == null)
                return null;
            knuckle = GetOtherKnuckle(bone, knuckle);
        }
        return knuckle;
    }

    public void TryToMoveKnuckle(Knuckle targetKnuckle, Vector3 targetPos)
    {
        Knuckle staticKnuckle = GetStaticKnuckle(targetKnuckle, null);
        Vector3 staticPos = staticKnuckle.transform.position;

        TryToMoveBranch(targetKnuckle, staticKnuckle, targetPos, false);
        TryToMoveBranch(staticKnuckle, targetKnuckle, staticPos, true);
    }

    public void TryToMoveBranch(Knuckle startKnuckle, Knuckle endKnuckle, Vector3 targetPos, bool invertedDirection)
    {
        startKnuckle.Move(targetPos);

        Knuckle refKnuckle = startKnuckle;
        Bone bone = GetOtherBone(startKnuckle, null);
        if (bone == null)
            return;

        List<Bone> bonesPath = RecursivePath(startKnuckle, endKnuckle, null);
        Knuckle movedKnuckle = startKnuckle;
        Knuckle previousKnuckle = null;

        foreach (Bone b in bonesPath)
        {
            refKnuckle = movedKnuckle;
            movedKnuckle = GetOtherKnuckle(b, movedKnuckle);
            PropagateMove(movedKnuckle, refKnuckle, previousKnuckle, b, movedKnuckle.transform.position, invertedDirection);
            previousKnuckle = refKnuckle;
        }
    }

    public void PropagateMove(Knuckle movedKnuckle, Knuckle refKnuckle, Knuckle previousKnuckle, Bone bone, Vector3 targetPos, bool invertedDirection)
    {

        Vector3 pos = targetPos;

        pos = AdjustLength(movedKnuckle, refKnuckle, bone, targetPos);
        pos = AdjustAngle(movedKnuckle, refKnuckle, previousKnuckle, bone, pos, invertedDirection);
        
        movedKnuckle.Move(pos);
    }

    public Vector3 AdjustLength(Knuckle movedKnuckle, Knuckle refKnuckle, Bone bone, Vector3 targetPos)
    {
        return refKnuckle.transform.position + Vector3.Normalize(targetPos - refKnuckle.transform.position) * bone.sizeConstraint;
    }

    public Vector3 AdjustAngle(Knuckle movedKnuckle, Knuckle refKnuckle, Knuckle previousKnuckle, Bone bone, Vector3 targetPos, bool invertedDirection)
    {
        if (previousKnuckle == null)
            return targetPos;

        Vector3 v1 = refKnuckle.transform.position - previousKnuckle.transform.position;
        Vector3 v2 = targetPos - refKnuckle.transform.position;

        float v1Angle = Mathf.Atan2(v1.y, v1.x);
        float v2Angle = Mathf.Atan2(v2.y, v2.x);

        float angle = v2Angle - v1Angle;

        if (angle > Mathf.PI)
            angle -= 2 * Mathf.PI;

        if(angle < -Mathf.PI)
            angle += 2 * Mathf.PI;

        float maxAngle = invertedDirection? refKnuckle.maxAngleRrad : refKnuckle.maxAngleLrad;
        float minAngle = invertedDirection ? - refKnuckle.maxAngleLrad : - refKnuckle.maxAngleRrad;

        if (angle > minAngle && angle < maxAngle)
            return targetPos;

        angle = Mathf.Clamp(angle, minAngle, maxAngle);

        Vector3 clampedDirection = new Vector3(
            Mathf.Cos(v1Angle + angle),
            Mathf.Sin(v1Angle + angle),
            0
        );

        return refKnuckle.transform.position + Vector3.Normalize(clampedDirection) * bone.sizeConstraint;
    }
}
