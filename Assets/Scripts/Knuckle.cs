﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knuckle : MonoBehaviour {

    public bool statik;
    [Range(0, 180)]
    public float maxAngleR = 60;

    [Range(0, 180)]
    public float maxAngleL = 60;

    [HideInInspector]
    public float maxAngleRrad;
    [HideInInspector]
    public float maxAngleLrad;

    private static Color staticColor = Color.red;
    private static Color moveColor;
    private Material mat;

    private Skeleton skeleton;

    private void Awake()
    {
        skeleton = GameObject.FindGameObjectWithTag("skeleton").GetComponent<Skeleton>();
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        mat = renderer.material;
        moveColor = mat.GetColor("_EmissionColor");
    }
	
	// Update is called once per frame
	void Update () {
        UpdateColor();
        UpdateAngle();
    }

    private void OnMouseDrag()
    {
        if(!statik)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

            //Move(objPosition);
            skeleton.TryToMoveKnuckle(this, objPosition);
        }
    }

    public void Move(Vector3 newPos)
    {
        newPos.z = 0;
        transform.position = newPos;
    }

    private void UpdateColor()
    {
        if (statik)
        {
            mat.SetColor("_EmissionColor", staticColor);
        }
        else
        {
            mat.SetColor("_EmissionColor", moveColor);
        }
    }

    private void UpdateAngle()
    {
        maxAngleLrad = maxAngleL * Mathf.PI / 180;
        maxAngleRrad = maxAngleR * Mathf.PI / 180;
    }
}
